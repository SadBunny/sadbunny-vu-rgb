#include <FastLED.h>

#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define LED_PIN     3 // Data led for RGB
#define NUM_LEDS  138 // Should be an even number

#define PA_LEFT    A1 // Analog input pin for left channel
#define PA_RIGHT   A0 // Analog input pin for right channel

#define PA_POT_1   A2 // Trimpot knob 1
#define PA_POT_2   A3 // Trimpot knob 2
#define PA_POT_3   A4 // Trimpot knob 3
#define PA_POT_4   A5 // Trimpot knob 4

byte POT_VALUE_1 = 0;
byte POT_VALUE_2 = 0;
byte POT_VALUE_3 = 0;
byte POT_VALUE_4 = 0;

// Knob tweak candidates below :)
byte BRIGHTNESS            = 192; // 0-255! Output brightness
#define BRIGHTNESS_BG          0  // 0-255! Background brightness

#define VU_MODE_COUNT         4 // The number of VU MODES to cycle through
#define VU_MODE_START         1 // The VU MODE to start with after boot
#define VU_MODE_AUTOCYCLE     0 // Set to true (1) for autocycle

// MODE SETTINGS (note: mode count is 0-based!)
#define MODE_1_HUE_INTERVAL  10 // Color cycle strength

float PI_DAMPING = .003;    // Peak indicator damping - the speed at which the peak indicator falls. I like 0.003.
float VU_DAMPING = .1;      // VU damping - the speed at which the VU's fall. I like 0.06.
int VU_PI_DIM_FACTOR = 300; // VU dimming factor - vu brightness will dim relative to peak indicator recession. This gives a more lively effect. Higher = faster dimming.

float OFFSET[2];
float AMPL[2];

float vu[2];
float vuRecede[2];
byte vuMode = VU_MODE_START;
byte vuMode1HueStart[2];
byte mode3hueOffset=0;

byte mode3arr[NUM_LEDS];

CHSV COLOR_BACKGROUND   = CHSV( 64,   0, BRIGHTNESS_BG);  // YELLOW: 64 // ... background color for all "inactive" indicator leds

// Percentages for determining whether the level is low, medium and high (determines where the low/med/high colors are split)
float PERCENT_LOW = 45; // Leds below PERCENT_LOW percentage will be colored with COLOR_LOW
float PERCENT_MED = 79; // Leds between PERCENT_LOW and PERCENT_MED will be colored with COLOR_MED. Everything higher than this will be COLOR_HIGH.

// Max value for analog value reading.
float MAX_INPUT_VAL = 1024;

CRGBArray<NUM_LEDS> leds;

int hue;

byte PIN_ANALOG[2];

void setup()
{
  PIN_ANALOG[0] = PA_LEFT;
  PIN_ANALOG[1] = PA_RIGHT;
  pinMode(PA_LEFT, INPUT);
  pinMode(PA_RIGHT, INPUT);
  pinMode(PA_POT_1, INPUT_PULLUP);
  pinMode(PA_POT_2, INPUT_PULLUP);
  pinMode(PA_POT_3, INPUT_PULLUP);
  pinMode(PA_POT_4, INPUT_PULLUP);
  OFFSET[0] = 0; // Left analog read offset (will be added to each analog readout)
  OFFSET[1] = 0; // Right analog read offset (will be added to each analog readout)
  AMPL[0] = 2; // Amplification of left channel level
  AMPL[1] = 2; // Amplification of right channel level

  randomSeed(analogRead(PA_LEFT));

  initMode3Array();
  
  Serial.begin(9600);

  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS);
//  FastLED.setBrightness(BRIGHTNESS);

  // Flash quickly to indicate startup
  for (int l = 0; l <= 3; l++) {
    for (CRGB & pixel : leds) {
      pixel = CHSV(hue++, 255, 64);
    }
    FastLED.show();
    delay(50);
  }
  for (CRGB & pixel : leds) {
    pixel = CRGB::Black;
  }
  FastLED.show();
  delay(150);
}

// Global variables and precalculations
char buff[120];

int i;

int count = 0;

int channelSplit = NUM_LEDS / 2;

int lowLimit = int(PERCENT_LOW / 100 * channelSplit);
int medLimit = int(PERCENT_MED / 100 * channelSplit);

int NLM1 = NUM_LEDS - 1;

int split[2];
int maxLeft = 0;
int maxRight = 0;

float vuMode1Inc[2];
float divs[2];

void readAudioInputs() {
  for (int i = 0; i <= 1; i++) {
    divs[i] = max(0, min(1, (analogRead(PIN_ANALOG[i]) + OFFSET[i]) / MAX_INPUT_VAL));
    split[i] = divs[i] * channelSplit * AMPL[i];
  }
}

float peakIndicator[2];
float peakIndicatorRecede[2];

void mode2HandlePixelLeft(CRGB &pixel, float strengthHueFactor, float strength, int i, byte saturation, byte value) {
  byte hue = i * strengthHueFactor;
  pixel.setHSV(hue, saturation, value);
}

void mode2HandlePixelRight(CRGB &pixel, float strengthHueFactor, float strength, int i, byte saturation, byte value) {
  byte hue = (i - channelSplit) * strengthHueFactor;
  pixel.setHSV(hue, saturation, value);
}

void mode3HandlePixel(CRGB &pixel, float strength, int i) {
  byte hue = strength * ((int)i%30) + mode3hueOffset;
  byte saturation = 255 - (int)i%20 - strength*20;
  byte value = min(255, 12 + 0.95 * strength * BRIGHTNESS * (i*1.5)/NUM_LEDS);
  pixel.setHSV(hue, saturation, value);
}

void fillLeftMeterLeds() {
  if (vuMode == 0) {
    if (vu[0] > 0) {
      for (CRGB & pixel : leds(0, min(vu[0], lowLimit - 1))) {
        pixel = CHSV(160, 255, max(0, BRIGHTNESS - (peakIndicatorRecede[1] * VU_PI_DIM_FACTOR)));     // BLUE:  160
      }
      if (vu[0] > lowLimit) {
        for (CRGB & pixel : leds(lowLimit, min(vu[0], medLimit - 1))) {
          pixel = CHSV( 96, 255, max(0, BRIGHTNESS - (peakIndicatorRecede[1] * VU_PI_DIM_FACTOR)));     // GREEN:  96
        }
        if (vu[0] > medLimit) {
          for (CRGB & pixel : leds(medLimit, min(vu[0], channelSplit))) {
            pixel = CHSV(  0, 255, max(0, BRIGHTNESS - (peakIndicatorRecede[1] * VU_PI_DIM_FACTOR)));     // RED:     0
          }
        }
      }
    }
  } else if (vuMode == 1) {
    byte hue_l = vuMode1HueStart[0];
    for (CRGB & pixel : leds(0, vu[0])) {
      hue_l += MODE_1_HUE_INTERVAL;
      byte piFactor_l = max(0, BRIGHTNESS - (peakIndicatorRecede[1] * VU_PI_DIM_FACTOR));
      pixel = CHSV(hue_l, min(piFactor_l * 2 + 50, 255), piFactor_l);
    }
  } else if (vuMode == 2) {
    float strength = vu[0]/channelSplit;
    float strengthHueFactor = strength*4.8 + .2;
    byte saturation = strength > .98 ? 0 : 192 + (strength * 63);
    byte value = 12 + 0.95 * strength * BRIGHTNESS;
    for (int i = 0; i<channelSplit; i++) {
      mode2HandlePixelLeft(leds[channelSplit-1-i], strengthHueFactor, strength, i, saturation, value);
    }
  } else if (vuMode == 3) {
    float strength = vu[0]/channelSplit;
    for (int i = 0; i<channelSplit; i++) {
      mode3HandlePixel(leds[mode3arr[i]], strength, i);
    }
  }
}

void fillRightMeterLeds() {
  if (vuMode == 0) {
    if (vu[1] > 0) {
      for (CRGB & pixel : leds(NLM1, NLM1 - min(vu[1], lowLimit - 1))) {
        pixel = CHSV(160, 255, max(0, BRIGHTNESS - (peakIndicatorRecede[1] * VU_PI_DIM_FACTOR)));     // BLUE:  160
      }
      if (vu[1] > lowLimit) {
        for (CRGB & pixel : leds(NLM1 - lowLimit, NLM1 - min(vu[1] - 1, medLimit - 1))) {
          pixel = CHSV( 96, 255, max(0, BRIGHTNESS - (peakIndicatorRecede[1] * VU_PI_DIM_FACTOR)));     // GREEN:  96
        }
        if (vu[1] > medLimit) {
          for (CRGB & pixel : leds(NLM1 - medLimit, NLM1 - min(vu[1] - 1, channelSplit - 1))) {
            pixel = CHSV(  0, 255, max(0, BRIGHTNESS - (peakIndicatorRecede[1] * VU_PI_DIM_FACTOR)));     // RED:     0
          }
        }
      }
    }
  } else if (vuMode == 1) {
    byte hue_r = vuMode1HueStart[1];
    for (CRGB & pixel : leds(NLM1, NLM1 - vu[1])) {
      hue_r += MODE_1_HUE_INTERVAL;
      byte piFactor_r = max(0, BRIGHTNESS - (peakIndicatorRecede[1] * VU_PI_DIM_FACTOR));
      pixel = CHSV(hue_r, min(piFactor_r * 2 + 50, 255), piFactor_r); // BLUE:  160
    }
  } else if (vuMode == 2) {
    float strength = vu[1]/channelSplit;
    float strengthHueFactor = strength*4.8 + .2;
    byte value = 12 + 0.95 * strength * BRIGHTNESS;
    byte saturation = strength > .98 ? 0 : 192 + (strength * 63);
    for (i = NLM1; i>=channelSplit; i--) {
      mode2HandlePixelRight(leds[i], strengthHueFactor, strength, i, saturation, value);
    }
  } else if (vuMode == 3) {
    float strength = vu[1]/channelSplit;
    for (i = NLM1; i>=channelSplit; i--) {
      mode3HandlePixel(leds[mode3arr[i]], strength, i);
    }
  }
}

void setPeakIndicator() {
  for (byte pi = 0; pi <= 1; pi++) {
    if (vu[pi] >= peakIndicator[pi]) {
      peakIndicator[pi] = vu[pi];
      peakIndicatorRecede[pi] = 0;
    }

    if (peakIndicator[pi] > 0) {
      peakIndicatorRecede[pi] += PI_DAMPING;
      peakIndicator[pi] -= peakIndicatorRecede[pi];
    } else {
      peakIndicatorRecede[pi] = 0;
      peakIndicator[pi] = 0;
    }
  }

  CHSV colorPi = CHSV(0, 0, min(255, BRIGHTNESS * 1.3));
  leds[int(peakIndicator[0] + 0.5)] = colorPi;
  leds[NLM1 - peakIndicator[1]] = colorPi;
}

void fillBackground() {
  for (CRGB & pixel : leds(vu[0], NLM1 - vu[1])) {
    pixel = COLOR_BACKGROUND;
  }
}

void setVu() {
  for (byte vui = 0; vui <= 1; vui++) {
    if (split[vui] > int(vu[vui])) {
      vu[vui] = split[vui];
      vuRecede[vui] = 0;
    }

    if (vu[vui] > 0) {
      vuRecede[vui] += VU_DAMPING; // += for quadratic VU drop, = for linear drop. I like quadratic.
      vu[vui] -= vuRecede[vui];
    } else {
      vuRecede[vui] = 0;
      vu[vui] = 0;
    }
    vu[vui] = max(0, vu[vui]);
    vu[vui] = min(vu[vui], channelSplit - 1);
  }
}

int oldVu[2];

void performVuMode1() {
  for (int i = 0; i <= 1; i++) {
    if (oldVu[i] > vu[i] * 1.1 || vu[i] > channelSplit * .9) {
      vuMode1Inc[i] = -5;
//      vuMode1Inc[i] = max(vuMode1Inc[i] + 4, 6);
    }
    vuMode1Inc[i] = min(vuMode1Inc[i] + .3, 5);
    vuMode1HueStart[i] += vuMode1Inc[i];
    oldVu[i] = vu[i];
  }
}

byte prevVuMode;

void initMode3Array() {
  for (int i=0; i<NUM_LEDS; i++) {
    mode3arr[i] = i;
  }

  for (int i = 0; i < NUM_LEDS - 1; i++)
  {
      int j = random(0, NUM_LEDS - i);
  
      int t = mode3arr[i];
      mode3arr[i] = mode3arr[j];
      mode3arr[j] = t;
  }
}

void mode3SwitchPixel() {
  float strength = vu[0]/channelSplit;
  if (strength > .95) { mode3hueOffset += 20; }
  for (int k = 0; k<int(strength*3 * strength*3); k++) {
    mode3hueOffset -= int(strength * 2);
    int i = random(0, NUM_LEDS - 1);
    int j = random(0, NUM_LEDS - 1);
    int t = mode3arr[j];
    mode3arr[j] = mode3arr[i];
    mode3arr[i] = t;
  }
}

void loop() {
  if ( count % 32 == 0) {
    POT_VALUE_1 = min(255, max(0, analogRead(PA_POT_1)));
    POT_VALUE_2 = min(255, max(0, analogRead(PA_POT_2)));
    POT_VALUE_3 = min(255, max(0, analogRead(PA_POT_3)));
    POT_VALUE_4 = min(255, max(0, analogRead(PA_POT_4)));

    BRIGHTNESS = POT_VALUE_1;

    AMPL[0] = (float) POT_VALUE_2 / 32;
    AMPL[1] = (float) POT_VALUE_2 / 32;

    vuMode = int(POT_VALUE_3 / (255 / VU_MODE_COUNT)); 
  }

  count = (count + 1) % 16384;

  if ( VU_MODE_AUTOCYCLE && count % 2048 == 0 ) {
    vuMode = (vuMode + 1) % VU_MODE_COUNT;
  }

  if (vuMode == 3 && prevVuMode != 3) {
    initMode3Array();
  }

  readAudioInputs();

  if ( vuMode == 1) {
    performVuMode1();
  }

  setVu();

  if (vuMode == 0 || vuMode == 1) {
    fillBackground();
  }

  fillLeftMeterLeds();
  fillRightMeterLeds();

  if (vuMode != 3) {
    setPeakIndicator();
  }

  if (vuMode == 3) {
    mode3SwitchPixel();
  }

  FastLED.show();

  if (prevVuMode != vuMode) {
    Serial.println("Mode changed from " + (String) prevVuMode + " to " + (String) vuMode);
    prevVuMode = vuMode;
  }
}
